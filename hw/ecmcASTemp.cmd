############################################################
############# NCL ASTEMP 

# 1  0:0   PREOP  +  EK1100-0010 EtherCAT-Koppler (2A E-Bus, FX-SingleMode, ID-Switc
# 2  0:1   PREOP  +  EL3202-0010 2K. Ana. Eingang PT100 (RTD), hochgenau
# 3  0:2   PREOP  +  EL3202-0010 2K. Ana. Eingang PT100 (RTD), hochgenau
# 4  0:3   PREOP  +  EL9410 E-Bus Netzteilklemme (Diagnose)
# 5  0:4   PREOP  +  EL7041-0052 1Ch. Stepper motor output stage (50V, 5A)
# 6  0:5   PREOP  +  EL7041-0052 1Ch. Stepper motor output stage (50V, 5A)
# 7  0:6   PREOP  +  EL7041-0052 1Ch. Stepper motor output stage (50V, 5A)
# 8  0:7   PREOP  +  EL7041-0052 1Ch. Stepper motor output stage (50V, 5A)


ecmcEpicsEnvSetCalc("ASTEMP_NUM", "0")

# SLAVE 0
#Configure EL1100 EtherCAT Coupler
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "0")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=$(ECMC_SLAVE_NUM), HW_DESC=EK1100"

# SLAVE 1
## Configure EL3202-0010 PT100
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "1")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=1, HW_DESC=EL3202-0010"
#
## Configure channel 1 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8000")
${SCRIPTEXEC} $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX_NCL.cmd

# Configure channel 2 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8010")
${SCRIPTEXEC} $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX_NCL.cmd

# SLAVE 2
## Configure EL3202-0010 PT100
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "2")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=2, HW_DESC=EL3202-0010"
#
## Configure channel 1 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8000")
${SCRIPTEXEC} $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX_NCL.cmd
# Configure channel 2 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8010")
${SCRIPTEXEC} $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX_NCL.cmd

#Configure EL70471-0052 stepper drive terminal
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=4, HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-52.200.2.5"
#
# Configure reduced current 0 mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

#Configure EL70471-0052 stepper drive terminal
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=5, HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-52.200.2.5"
#
# Configure reduced current 0 mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

#Configure EL70471-0052 stepper drive terminal
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=6, HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-52.200.2.5"
#
# Configure reduced current 0 mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

#Configure EL70471-0052 stepper drive terminal
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=7, HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-52.200.2.5"
#
# Configure reduced current 0 mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"