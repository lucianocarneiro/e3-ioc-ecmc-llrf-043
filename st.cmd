##############################################################################
## EtherCAT Motion Control Acc Stubs Temperature Measurement

##############################################################################

require essioc
require ecmccfg, 8.0.0

## Initiation:
epicsEnvSet("ECMC_VER" ,"8.0.0")
epicsEnvSet("IOC" ,"$(IOC="MBL-020:Ctrl")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=$(ECMC_VER="8.0.0"),NAMING=ESSnaming,stream_VER=2.8.22,EC_RATE=200,ECMC_ASYN_PORT_MAX_PARAMS=10000"

##############################################################################
# Configure hardware:

ecmcFileExist($(E3_CMD_TOP)/hw/ecmcASTemp.cmd,1)
# NCL 1
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcASTemp.cmd

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

# ADDITIONAL SETUP
# Set all outputs to feed switches
#ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM_DIG_OUT},BO_1,1)"
# END of ADDITIONAL SETUP

$(SCRIPTEXEC) $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=0, SAMPLE_RATE_MS=3000,FILE=$(E3_CMD_TOP)/plc/autodisable.plc, PLC_MACROS='PLC_ID=0,DBG='")

epicsEnvSet("DEV",      "$(IOC)")
##############################################################################
## AXIS 1
#
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_1.ax)

ecmcConfigOrDie "Cfg.SetAxisEnableMotionFunctions(4,1,0,1)"

##############################################################################
## AXIS 2
#
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_2.ax)

ecmcConfigOrDie "Cfg.SetAxisEnableMotionFunctions(5,1,0,1)"

##############################################################################
## AXIS 3
#
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_3.ax)

ecmcConfigOrDie "Cfg.SetAxisEnableMotionFunctions(6,1,0,1)"

##############################################################################
## AXIS 4
#
$(SCRIPTEXEC) ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_4.ax)

ecmcConfigOrDie "Cfg.SetAxisEnableMotionFunctions(7,1,0,1)"

##############################################################################

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"

ecmcConfigOrDie "Cfg.SetDiagAxisIndex(4)"
ecmcConfigOrDie "Cfg.SetDiagAxisIndex(5)"
ecmcConfigOrDie "Cfg.SetDiagAxisIndex(6)"
ecmcConfigOrDie "Cfg.SetDiagAxisIndex(7)"

ecmcConfigOrDie "Cfg.SetDiagAxisFreq(2)"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)

iocInit()

